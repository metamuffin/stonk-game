use std::{error::Error, fs::File, io::stdin, thread, time::Duration};
use stonk_common::{DataPoint, NetworkPacket, Series};

fn main() {
    let symbol = std::env::args().skip(1).next().unwrap();
    let (out_tx, in_rx) = stonk_common::network_stuff();
    println!("press enter to start thee game");
    let mut l = String::new();
    stdin().read_line(&mut l).unwrap();
    println!("now ingame");
    out_tx
        .send(NetworkPacket::InitInfo {
            symbol: symbol.clone(),
        })
        .unwrap();

    let out_tx2 = out_tx.clone();
    let s2 = symbol.clone();
    thread::spawn(move || {
        let s = load(&s2).unwrap();
        for p in s.values {
            out_tx2.send(NetworkPacket::ExtendSeries(p)).unwrap();
            out_tx2.send(NetworkPacket::Tick).unwrap();
            thread::sleep(Duration::from_millis(250))
        }
    });

    for p in in_rx {
        match p {
            NetworkPacket::Join(_) => {
                out_tx
                    .send(NetworkPacket::InitInfo {
                        symbol: symbol.clone(),
                    })
                    .unwrap();
            }
            _ => {}
        }
    }
}

pub fn load(symbol: &str) -> Result<Series, Box<dyn Error>> {
    let f = File::open(format!("data/{symbol}")).unwrap();
    let mut f = csv::Reader::from_reader(f);
    let mut values = vec![];
    for result in f.deserialize() {
        let record: DataPoint = result?;
        values.push(record)
    }
    Ok(Series {
        symbol: symbol.to_owned(),
        values: values.into_iter().rev().collect(),
    })
}
