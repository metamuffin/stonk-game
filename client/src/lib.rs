pub mod ingame;
pub mod network;

#[cfg(target_arch = "wasm32")]
use eframe::wasm_bindgen::{self, prelude::*};

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn start(canvas_id: &str) -> Result<(), eframe::wasm_bindgen::JsValue> {
    console_error_panic_hook::set_once();
    tracing_wasm::set_as_global_default();
    use log::{debug, error};
    use crate::App;
    eframe::start_web(
        canvas_id,
        Box::new(|cc| Box::new(App::new(cc, "test".to_string()))),
    )
}

use crossbeam_channel::{Receiver, Sender};
use eframe::CreationContext;
use egui::FontId;
use std::collections::HashMap;
use stonk_common::{NetworkPacket, PlayerState, Series};

pub struct App {
    pub running: bool,
    pub in_rx: Receiver<NetworkPacket>,
    pub out_tx: Sender<NetworkPacket>,
    pub name: String,
    pub s: Series,
    pub player_states: HashMap<String, PlayerState>,
    pub price: f64,
}

impl App {
    pub fn new(cc: &CreationContext, name: String) -> Self {
        let (out_tx, in_rx) = stonk_common::network_stuff();
        out_tx.send(NetworkPacket::Join(name.clone())).unwrap();

        cc.egui_ctx.set_visuals(egui::Visuals::dark());
        let mut style = (*cc.egui_ctx.style()).clone();
        style.text_styles.insert(
            egui::TextStyle::Body,
            FontId::new(24.0, egui::FontFamily::Proportional),
        );
        style.text_styles.insert(
            egui::TextStyle::Button,
            FontId::new(32.0, egui::FontFamily::Proportional),
        );
        cc.egui_ctx.set_style(style);

        let mut player_states = HashMap::new();
        player_states.insert(name.clone(), PlayerState::default());
        App {
            name,
            running: false,
            price: 999999999999.0,
            player_states,
            s: Series {
                symbol: String::new(),
                values: vec![],
            },
            in_rx,
            out_tx,
        }
    }
}
