use crate::App;
use egui::{
    plot::{BoxElem, BoxPlot, BoxSpread, Legend, Plot},
    Align2, Color32, Stroke, Ui, Vec2,
};
use egui_extras::TableBuilder;
use stonk_common::{PlayerState, Series};

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        self.network();

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.ctx().request_repaint();

            if !self.running {
                egui::Window::new("Waiting")
                    .anchor(Align2::CENTER_CENTER, Vec2::ZERO)
                    .show(ui.ctx(), |ui| {
                        ui.heading("Waiting for the game to start...")
                    });
            }

            egui::Window::new("Info")
                .resizable(false)
                .show(ui.ctx(), |ui| self.hud(ui));
            egui::Window::new("Actions").show(ui.ctx(), |ui| {
                ui.add_enabled_ui(self.running, |ui| self.actions(ui))
            });
            egui::Window::new("Players").show(ui.ctx(), |ui| self.player_info(ui));
            ui.add_enabled_ui(self.running, |ui| {
                Plot::new("Chart")
                    .legend(Legend::default())
                    .show(ui, |plot_ui| {
                        plot_ui.box_plot(self.s.plot());
                    });
            })
        });
    }
}

impl App {
    pub fn own_state_mut(&mut self) -> &mut PlayerState {
        self.player_states.get_mut(&self.name).unwrap()
    }
    pub fn own_state(&self) -> &PlayerState {
        self.player_states.get(&self.name).unwrap()
    }

    pub fn hud(&mut self, ui: &mut Ui) {
        TableBuilder::new(ui)
            .columns(egui_extras::Size::exact(140.0), 2)
            .body(|mut body| {
                let mut r = |label: &str, s: &str| {
                    body.row(30.0, |mut row| {
                        row.col(|ui| {
                            ui.label(label);
                        });
                        row.col(|ui| {
                            ui.label(s);
                        });
                    });
                };
                r(
                    "Date:",
                    &self
                        .s
                        .values
                        .iter()
                        .last()
                        .map(|e| e.clone())
                        .unwrap_or_default()
                        .timestamp,
                );
                r("Price:", &format!("{:.2}$", self.price));
            })
    }

    pub fn player_info(&mut self, ui: &mut Ui) {
        TableBuilder::new(ui)
            .column(egui_extras::Size::remainder().at_least(80.0))
            .columns(egui_extras::Size::exact(140.0), 3)
            .header(30.0, |mut header| {
                header.col(|ui| {
                    ui.heading("Name");
                });
                header.col(|ui| {
                    ui.heading("Funds");
                });
                header.col(|ui| {
                    ui.heading("Stock");
                });
                header.col(|ui| {
                    ui.heading("Worth");
                });
            })
            .body(|mut body| {
                let best_w = self
                    .player_states
                    .values()
                    .map(|v| v.funds + v.stocks * self.price)
                    .reduce(|a, b| if a > b { a } else { b })
                    .unwrap();
                for (name, state) in &self.player_states {
                    let worth = state.funds + state.stocks * self.price;
                    let color = match (name == &self.name, worth == best_w) {
                        (true, true) => Color32::YELLOW,
                        (true, false) => Color32::LIGHT_GREEN,
                        (false, true) => Color32::LIGHT_BLUE,
                        (false, false) => Color32::LIGHT_GRAY,
                    };
                    body.row(30.0, |mut row| {
                        row.col(|ui| {
                            ui.colored_label(color, name);
                        });
                        row.col(|ui| {
                            ui.colored_label(color, format!("{:.2}$", state.funds));
                        });
                        row.col(|ui| {
                            ui.colored_label(color, format!("{:.0}", state.stocks));
                        });
                        row.col(|ui| {
                            ui.colored_label(color, format!("{:.2}$", worth));
                        });
                    });
                }
            });
    }

    pub fn transaction_possible(&mut self, amount: f64) -> bool {
        self.own_state_mut().funds >= amount * self.price
            && amount + self.own_state_mut().stocks >= 0.0
    }

    pub fn transaction(&mut self, amount: f64) {
        self.own_state_mut().stocks += amount;
        self.own_state_mut().funds -= amount * self.price;
    }

    pub fn actions(&mut self, ui: &mut Ui) {
        let max_buy = (self.own_state_mut().funds / self.price).floor();
        let max_sell = -self.own_state_mut().stocks;
        for a in [
            Some(max_buy),
            Some(100.0),
            Some(10.0),
            Some(1.0),
            None,
            Some(-1.0),
            Some(-10.0),
            Some(-100.0),
            Some(max_sell),
        ] {
            match a {
                None => {
                    ui.colored_label(Color32::YELLOW, "--------------");
                }
                Some(a) => {
                    ui.add_enabled_ui(self.transaction_possible(a), |ui| {
                        if ui
                            .button(format!(
                                "{} {:.0}",
                                if a < 0.0 { "Sell" } else { "Buy" },
                                a.abs()
                            ))
                            .clicked()
                        {
                            self.transaction(a)
                        }
                    });
                }
            }
        }
    }
}

trait Plottable {
    fn plot(&self) -> BoxPlot;
}
impl Plottable for Series {
    fn plot(&self) -> BoxPlot {
        BoxPlot::new(
            self.values
                .iter()
                .enumerate()
                .map(|(i, e)| {
                    let color = if e.open > e.close {
                        Color32::RED
                    } else {
                        Color32::GREEN
                    };
                    BoxElem::new(
                        i as f64,
                        BoxSpread::new(e.low, e.open, (e.open + e.close) / 2.0, e.close, e.high),
                    )
                    .stroke(Stroke::new(1.5, color))
                    .fill(color.linear_multiply(0.2))
                })
                .collect(),
        )
        .name(&self.symbol)
    }
}
