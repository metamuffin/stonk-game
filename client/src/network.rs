use stonk_common::NetworkPacket;

use crate::App;

impl App {
    pub fn network(&mut self) {
        for p in self.in_rx.try_iter() {
            match p {
                NetworkPacket::InitInfo { symbol } => {
                    self.s.symbol = symbol;
                    self.running = true;
                }
                NetworkPacket::BroadcastState(name, state) => {
                    self.player_states.insert(name, state);
                }
                NetworkPacket::ExtendSeries(p) => {
                    self.price = p.close;
                    self.s.values.push(p);
                }
                NetworkPacket::Tick => {
                    self.out_tx
                        .send(NetworkPacket::BroadcastState(
                            self.name.clone(),
                            self.own_state().clone(),
                        ))
                        .unwrap();
                }
                _ => {}
            };
        }
    }
}
