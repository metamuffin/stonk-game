use stonk_client::App;

#[cfg(not(target_arch = "wasm32"))]
fn main() {
    env_logger::init();
    let name = String::from(std::env::args().skip(1).next().unwrap());

    let options = eframe::NativeOptions::default();
    eframe::run_native(
        "stonks game",
        options,
        Box::new(|cc| Box::new(App::new(cc, name))),
    );
}

#[cfg(target_arch = "wasm32")]
fn main() {}
