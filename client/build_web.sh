#!/usr/bin/env bash
set -eu

CRATE_NAME=stonk-client

script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$script_path"

OPEN=false
OPTIMIZE=false

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "build_web.sh [--optimize] [--open]"
      echo "  --optimize: enable optimization step"
      echo "  --open: open the result in a browser"
      exit 0
      ;;
    -O|--optimize)
      shift
      OPTIMIZE=true
      ;;
    --open)
      shift
      OPEN=true
      ;;
    *)
      break
      ;;
  esac
done

./setup_web.sh

CRATE_NAME_SNAKE_CASE="${CRATE_NAME//-/_}" # for those who name crates with-kebab-case

export RUSTFLAGS=--cfg=web_sys_unstable_apis

# Clear output from old stuff:
rm -f "out/${CRATE_NAME}_bg.wasm"

echo "Building rust…"
BUILD=release
cargo build -p "${CRATE_NAME}" --release --lib --target wasm32-unknown-unknown

# Get the output directory (in the workspace it is in another location)
TARGET=$(cargo metadata --format-version=1 | jq --raw-output .target_directory)

echo "Generating JS bindings for wasm…"
TARGET_NAME="${CRATE_NAME}.wasm"
WASM_PATH="${TARGET}/wasm32-unknown-unknown/${BUILD}/${TARGET_NAME}"
wasm-bindgen "${WASM_PATH}" --out-dir out --no-modules --no-typescript

if [[ "${OPTIMIZE}" == true ]]; then
  echo "Optimizing wasm…"
  wasm-opt "out/${CRATE_NAME}_bg.wasm" -O2 --fast-math -o "out/${CRATE_NAME}_bg.wasm" # add -g to get debug symbols
fi

