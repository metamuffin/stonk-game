use crossbeam_channel::{Receiver, Sender};
use serde::{Deserialize, Serialize};
use std::io::{BufRead, BufReader, Write};
use std::net::TcpStream;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Series {
    pub symbol: String,
    pub values: Vec<DataPoint>,
}

#[derive(Debug, Deserialize, Serialize, Clone, Default)]
pub struct DataPoint {
    pub timestamp: String,
    pub open: f64,
    pub high: f64,
    pub low: f64,
    pub close: f64,
    pub adjusted_close: f64,
    pub volume: f64,
    pub dividend_amount: f64,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct PlayerState {
    pub funds: f64,
    pub stocks: f64,
}

impl Default for PlayerState {
    fn default() -> Self {
        Self {
            funds: 1000.0,
            stocks: Default::default(),
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum NetworkPacket {
    Join(String),
    Ready(String, bool),
    InitInfo { symbol: String },
    ExtendSeries(DataPoint),
    Tick,
    BroadcastState(String, PlayerState),
}

pub fn packet_de(s: &str) -> Option<NetworkPacket> {
    serde_json::from_str(s).ok()
}
pub fn packet_ser(p: &NetworkPacket) -> String {
    serde_json::to_string(p).unwrap()
}

#[cfg(not(target_arch = "wasm32"))]
use std::thread;
#[cfg(target_arch = "wasm32")]
use wasm_thread as thread;

#[cfg(target_arch = "wasm32")]
pub fn network_stuff() -> (Sender<NetworkPacket>, Receiver<NetworkPacket>) {
    use ewebsock::{WsEvent, WsMessage};
    use log::{debug, error, info};

    let (in_tx, in_rx) = crossbeam_channel::unbounded();
    let (out_tx, out_rx) = crossbeam_channel::unbounded();

    thread::spawn(move || loop {
        let (mut send, recv) = ewebsock::connect("ws://metamuffin.org:1235/").unwrap();
        if let Some(ev) = recv.try_recv() {
            match ev {
                WsEvent::Opened => info!("websocket open"),
                WsEvent::Message(msg) => {
                    if let WsMessage::Text(t) = msg {
                        if let Some(p) = packet_de(&t) {
                            debug!("<-  {p:?}");
                            // in_tx.send(p).unwrap()
                        }
                    }
                }
                WsEvent::Error(e) => error!("websocket error: {e}"),
                WsEvent::Closed => error!("websocket closed"),
            }
            for p in out_rx.try_iter() {
                debug!(" ->  {p:?}");
                send.send(WsMessage::Text(format!("{}\n", packet_ser(&p))));
            }
            thread::sleep(std::time::Duration::from_millis(50));
        }
    });

    (out_tx, in_rx)
}

#[cfg(not(target_arch = "wasm32"))]
pub fn network_stuff() -> (Sender<NetworkPacket>, Receiver<NetworkPacket>) {
    let (in_tx, in_rx) = crossbeam_channel::unbounded();
    let (out_tx, out_rx) = crossbeam_channel::unbounded();

    let mut conn = TcpStream::connect("metamuffin.org:1234").unwrap();
    let conn2 = conn.try_clone().unwrap();
    thread::spawn(move || {
        let mut reader = BufReader::new(conn2);
        let mut line = String::new();
        loop {
            line.clear();
            reader.read_line(&mut line).unwrap();
            line.pop(); // remove lf
            if let Some(p) = packet_de(&line) {
                println!("<-  {p:?}");
                in_tx.send(p).unwrap()
            }
        }
    });
    thread::spawn(move || {
        for p in out_rx {
            println!(" ->  {p:?}");
            conn.write_fmt(format_args!("{}\n", packet_ser(&p)))
                .unwrap();
        }
    });

    (out_tx, in_rx)
}
